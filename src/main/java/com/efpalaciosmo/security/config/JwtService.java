package com.efpalaciosmo.security.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtService {
    private static final String SECRET_KEY = "4D635166546A576E5A7234753778214125442A462D4A614E645267556B587032";
    public String extractUserEmail(String token) { // get the subject (email) on the token
        return extractClaims(token, Claims::getSubject);
    }
    public String generateToken(UserDetails userDetails){ // generate a token without extra claims
        return generateToken(new HashMap<>(), userDetails);
    }
    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails){ // generate a token for auth
        return Jwts.builder().setClaims(extraClaims)
                .setSubject(userDetails.getUsername()) // Should be getEmail but for spring is always username
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()+ 1000 * 60 * 24)) // is valid for 24 hours plus 1000 milliseconds
                .signWith(getSignKey(), SignatureAlgorithm.HS256).compact();
    }

    public boolean isTokenValid(String token, UserDetails userDetails){ // validate if the given token is right
        final String email = extractUserEmail(token);
        return (email.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
    private boolean isTokenExpired(String token) { // check if the token expired
        return extractExpiration(token).before(new Date());
    }
    private Date extractExpiration(String token) { // Get the expiration date for the token
        return extractClaims(token, Claims::getExpiration);
    }
    public <T> T extractClaims(String token, Function<Claims, T> claimsResolver){ // Extract all claims from any type
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token){ // Get all claims on the token
        return Jwts.parserBuilder()
                .setSigningKey(getSignKey()).build().parseClaimsJwt(token).getBody();
    }

    private Key getSignKey() { // Decode the secret key
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
