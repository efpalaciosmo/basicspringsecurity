## Features
- User registration and login with JWT authentication
- Password encryption using BCrypt
- Role-based authorization with Spring Security
- Customized access denied handling

## Technologies
- Spring Boot 3.0
- Spring Security
- JSON Web Token (JWT)
- BCrypt
- PostgreSQL
- Docker
- Gradle


## Setup 
To run use the database, run
```shell
podman pull postgres:15
podman run -d --name spring -e POSTGRES_PASSWORD=root -p 5432:5432 postgres:15
podman exec -it spring bash
psql -U postgres
CREATE DATABASE jwt_security;
```

- Clone the repository: git clone `https://gitlab.com/efpalaciosmo/basicspringsecurity`
- Navigate to the project directory: `cd BasicSpringSecurity`
- Run the project

The application will be available at `http://localhost:8080`